# Bank Accounting

Esse projeto tem como objetivo a construcao de uma api simples de transfernecia de dinheiro entre contas com autenticacao dos usuarios.

## Proposta de solucao

O projeto foi arquitetado seguindo principios do Domain Driven Design. Procurei separar toda a logica especifica do negocio em objetos simples de Ruby, separados do framework Rails, com o objeto de modularizar a aplicacao, facilitar a modelagem das funcionalidades especificas do negocio, diminuir o acoplamento entre a logica de negocios e o framework em si e com isso facilitar uma possivel extracao do modulo para outro servico, caso seja pertinente, com uma relativa facilidade.

Tambem procurei seguir o modelo de arquitetira de portas e adaptadores, onde as interacoes entre as camadas de infraestutura e aplicacao com o core do negocio ficam restrita a portas e adaptadores especificos como os casos de uso e os repositorios.

Por causa desses decisoes de arquitetura o projeto ficou com uma organizacao de pastas diferentes do convencional do Rails. Comecei construindo a logica de negocio com os objetos de dominio sem o rails e para isso criei uma pasta application que carrega esses objetos de dominio e as portas e adaptadores que servem de interface com o banco de dados e a camada de aplicacao web. Para isso criei uma pasta chamada  `/application` que fica na raiz do projeto e carrega a logica especifica do negocio e os testes unitarios para os objetos de dominio e casos de uso.

O Rails em si fica na pasta `/web-app` e la ficam encapsuladas as interacoes com o banco de dados e a camada web. Criei o projeto em Rails com VueJS com a ideia de no futuro criar o front-end em Vue e o banco de dados com PostgreSQL.

Tambem decidi seguir um padrao de arquitetura de eventos, mais especificamete eventos que transferem estado do objeto ou que notificam alguma acao. Para isso criei um `/event_manager` na pasta `/application` e nesse gerenciador de eventos estao construida as bases do publisher, dos eventos em si e dos handlers que serao acionados quando eventos especificos forem lancados. Na situacao atual, toda transacao publica eventos mas eles sao apenas guardados na tabela `Events`, porem a ideia é permitir expandir as acoes quando os eventos sao lancados para poder realizar alguma acao com uma certa facilidade, somente criando novos handlers e acoplando os handlers ao publisher. 

O projeto hoje nao possui uma API que expoe o recurso para criar novas contas, portanto para o ambiente local de testes eu criei um seed basico para criar usuarios e contas atreladas a esses usuarios.

Ao rodar os seeds, serao criados 10 com o email `0@a.com`, sendo q o pirmero dgito vai do 0 ao 9 e todos com a senha `password` e com saldos diferentes na conta.

Eu cheguei a iniciar a construcao do front end, mas como vai alem do escopo do projeto e como nao havia mais tempo, o front end nao esta funcional porem esta presente na branch `feature/front-end`

## Pré-requesitos

O projeto foi construido utilizando a versao `ruby 2.6.0` e `Rails 6.0.2.1`. O banco de dados escolhido foi o PostgreSQL na versao `psql 12.1`. Para testar a API utilzei o Postman como meio de emitir os requests.

## Instalacao

Depois instalar o ruby, o rails e o postgresql e de clonar o repositorio, se dirigir a pasta `/web-app` e rodar os comandos:
```
$ bundle install
$ yarn install (*)
$ rails db:create
$ rails db:migrate
$ rails db:seed
$ rails s
```
Assim o servidor comecara a expor a API no localhost na porta 3000.

(*) Apesar de nao estarmos utilizando nenhum pacote em JS, como inicei o projeto do rails com vuejs pensando em montar o frontend em vue, ele ja esta com webpacker e precisa dos node modules para rodar.

### Testes

Os testes foram separados em dois modulos com objetivos especificos. Dentro da pasta `/application` possui uma pasta `/application/spec` que contem os testes unitarios que foram criados para ajudar na modelagem e tambem para servir como as especificacoes da construcao dos objetos de dominio.

Dentro da pasta `/web-app` tambem exite uma outra pasta `/web-app/spec` que contem os testes de integracao da solucao. Os testes de integracao tem a funcao de garantir que o fluxo de funcionamento esta correto e que os controladores estao respondendo as requisicoes da maneira prevista.

Nos dois casos, para rodar os testes automaticamente, somente ir na pasta `/spec` de cada caso e rodar o comando
```
$ guard
```
E apertar a tecla `Enter` que os testes rodarao automaticamente.

## Autenticacao
Utilizo o padrao JSON web token para gerir a autenticacao dos usuarios. Escolhi a JWT pela relativa facilidade de implementacao e pelo fato de se adequar bem a API`s.

Toda transcao o usuario precisa estar em posse de um token valido, portanto todos os request (exceto os de criar usuario e login) necessitam que o token seja passado no header da requisicao no seguinte formato
```
Authorization:JSON_WEB_TOKEN_HASH
```
## TO DO
- Criar front end para consumir a api
- Arquitetar mecanismo para criacao de contas, talvez com um usuario tipo `admin` ou `gerente`
- Definir a maneira de realizar deploy e configurar o projeto para rodar em producao
- 
## Documentacao da API 

**Create User**
----
 Cria um usuario novo
* **URL**
  /api/v1/user
* **Method:**
  `POST`
*  **Body**
    ```
    { 
      "user": {
        "name": "Luiz Neiva",
        "username": "sou eu",
        "email": "luizgzn@gmail.com",
        "password": "123456"
      }
    }
    ```
* **Success Response:**
  * **Code:** 200
    **Content:** `{"message": "User created."}`
* **Error Response:**
  * **Code:** 400 BAD REQUEST
    **Content:** `{ error : "Validation error" }`

**Sign In User**
----
 Authenticate user
* **URL**
  /api/v1/auth/login
* **Method:**
  `POST`
*  **Body**
    ```
    { 
      "email": "luizgzn@gmail.com",
      "password": "password"
    }
    ```
* **Success Response:**
  * **Code:** 200 <br>
    **Content:** `{"token":"token hash","username":"usuario_0"}`
* **Error Response:**
  * **Code:** 401 UNAUTHORIZED
    **Content:** `{ "error": "Unauthorized" }`
  
** Show Account**
----
 Mostra a conta do usuario
* **URL**
api/v1/account/:id
* **Method:**
  `GET`
*  **Headers**
  `Authorization:JsonWebtokenHash`
* **Success Response:**
  * **Code:** 200 <br>
    **Content:** 
    ```
    {
      "account_data": {
          "id": 1,
          "balance": 0,
          "user_id": 1
      }
    }
    ``` 
* **Error Response:**
  * **Code:** 401 UNAUTHORIZED
    **Content:** `{ "error": "Unauthorized" }`

** Realiza transferencia **
----
 Transferir dinheiro entre contas
* **URL**
  api/v1/transfer_money
* **Method:**
  `POST`
*  **Headers**
  `Authorization:JsonWebtokenHash`
*  **Body**
    ```
    { 
      "source_account_id": 1,
      "destination_account_id": 42,
      "amount": 424242
    }
    ```
* **Success Response:**
  * **Code:** 200 <br>
    **Content:** 
    ```
    {
      "account_data": {
          "id": 1,
          "balance": 0,
          "user_id": 1
      }
    }
    ``` 
* **Error Response:**
  * **Code:** 401 UNAUTHORIZED
    **Content:** `{ "error": "Unauthorized" }`
* **Error Response:**
  * **Code:** 400 Bad Request
    **Content:** `{ "error": "Insuficient funds" }`