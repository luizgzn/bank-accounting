module AccountsManagement
  module Domain
    module Specifications
      class OriginalAccountSufficientFunds
        def check!(account:, amount:)
          balance = account.balance

          if !satisfied_by?(balance, amount)
            raise_insufficient_funds_error(account)
          end

          true
        end

        private

        def satisfied_by?(balance, amount)
          balance >= amount
        end

        def raise_insufficient_funds_error(account)
          raise AccountsManagement::Exceptions::InsufficientFundsException.new("The account #{account.id} does not have enough funds for this transaction")
        end
      end
    end
  end
end