module AccountsManagement
  module Domain
    class Account

      attr_reader :id, :balance, :user_id
    
      def initialize(id:, balance: 0, user_id: nil)
        is_integer?(balance)
        @id = id
        @balance = balance
        @user_id = user_id
      end

      def verify!(specifications:, amount:)
        specifications.each do |specification|
          specification.check!(account: self, amount: amount)
        end
      end

      def deposit!(amount)
        is_integer?(amount)
        @balance += amount
      end

      def withdraw!(amount)
        is_integer?(amount)
        @balance -= amount
      end
      
      def to_hash
        {
          id: self.id,
          balance: self.balance,
          user_id: self.user_id
        }
      end

      private

      def is_integer?(parameter)
        raise TypeError unless parameter.is_a?(Integer)
      end

    end
  end
end