module AccountsManagement
  module Exceptions
    class AccountNotFoundException < RuntimeError
    end
  end
end
