module AccountsManagement
  module Exceptions
    class AccountOwnershipException < RuntimeError
    end
  end
end