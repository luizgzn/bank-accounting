module AccountsManagement
  module Exceptions
    class InsufficientFundsException < RuntimeError
    end
  end
end
