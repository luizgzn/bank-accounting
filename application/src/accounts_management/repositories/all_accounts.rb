module AccountsManagement
  module Repositories
    class AllAccounts

      def get_by_id(account_id)
        begin
          account_model = Account.find(account_id)
          AccountsManagement::Domain::Account.new(
            balance: account_model.balance,
            id: account_model.id,
            user_id: account_model.user_id
          )
        rescue ActiveRecord::RecordNotFound => e
          raise AccountsManagement::Exceptions::AccountNotFoundException.new(e.message)
        end
      end

      def transaction
        Account.transaction do
          yield
        end
      end

      def create(balance:, user_id:)
        account_model = Account.create(balance: balance, user_id: user_id)
        AccountsManagement::Domain::Account.new(
          balance: account_model.balance,
          id: account_model.id,
          user_id: user_id
        )
      end

      def save(account)
        account_model = Account.find(account.id)
        account_model.balance = account.balance
        account_model.save!
      end

    end
  end
end