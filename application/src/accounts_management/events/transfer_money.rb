module AccountsManagement
  module Events
    class TransferMoney < EventManager::EventBase

      NAME = "AccountsManagement.TransferMoney"

      def initialize(source_account:, destination_account:, amount:)
        @source_account = source_account
        @destination_account = destination_account
        @amount = amount
      end

      def name
        NAME
      end

      def event_object_id
        @source_account.id
      end

      def event_object_type
        @source_account.class.name
      end

      def event_data
        build_event_data
      end

      private

      def build_event_data
        {
          source_account_balance_before_transaction: @source_account.balance + amount,
          destination_account_id: @destination_account.id,
          amount: @amount
        }
      end

    end
  end
end
