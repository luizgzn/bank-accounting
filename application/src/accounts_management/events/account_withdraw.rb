module AccountsManagement
  module Events
    class AccountWithdraw < EventManager::EventBase

      NAME = "AccountsManagement.AccountWithdraw"

      def initialize(account:, amount:)
        @account = account
        @amount = amount
      end

      def name
        NAME
      end

      def event_object_id
        @account.id
      end

      def event_object_type
        @account.class.name
      end

      def event_data
        build_event_data
      end

      private

      def build_event_data
        {
          account: @account.to_hash,
          amount: @amount
        }
      end

    end
  end
end
