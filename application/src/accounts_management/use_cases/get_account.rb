module AccountsManagement
  module UseCases
    class GetAccount

      def self.build
        all_accounts = AccountsManagement::Repositories::AllAccounts.new
        new(all_accounts: all_accounts)
      end

      def initialize(all_accounts:)
        @all_accounts = all_accounts
      end

      def execute(account_id:)
        @all_accounts.get_by_id(account_id)
      end

    end
  end
end