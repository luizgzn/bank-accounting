module AccountsManagement
  module UseCases
    class TransferMoney
      def self.build
        all_accounts = AccountsManagement::Repositories::AllAccounts.new
        specifications = [
          Domain::Specifications::OriginalAccountSufficientFunds.new,
        ]
        new(all_accounts: all_accounts, specifications: specifications)
      end

      def initialize(all_accounts:, specifications:)
        @all_accounts = all_accounts
        @specifications = specifications
      end

      def execute(source_account_id:, destination_account_id:, amount:)
        @all_accounts.transaction do
          source_account = @all_accounts.get_by_id(source_account_id)
          source_account.verify!(specifications: @specifications, amount: amount)
          destination_account = @all_accounts.get_by_id(destination_account_id)

          source_account.withdraw!(amount)
          publish_account_withdraw_event(account: source_account, amount: amount)
          
          destination_account.deposit!(amount)
          publish_account_deposit_event(account: destination_account, amount: amount)

          @all_accounts.save(source_account)
          @all_accounts.save(destination_account)

          publish_money_transfer_event(
            source_account: source_account, 
            destination_account: destination_account, 
            amount: amount
          )

          source_account
        end
      end

      private

      def publish_account_withdraw_event(account:, amount:)
        event = Events::AccountWithdraw.new(account: account, amount: amount)
        accounts_management_publisher.publish(event)
      end

      def publish_account_deposit_event(account:, amount:)
        event = Events::AccountDeposit.new(account: account, amount: amount)
        accounts_management_publisher.publish(event)
      end

      def publish_money_transfer_event(source_account:, destination_account:, amount:)
        event = Events::TransferMoney.new(source_account: source_account, destination_account: destination_account, amount: amount)
        accounts_management_publisher.publish(event)
      end
      
      def accounts_management_publisher
        EventManager::PublisherFactory.build_with_subscribers
      end

    end
  end
end