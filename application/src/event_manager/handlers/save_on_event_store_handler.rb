module EventManager
  module Handlers

    class SaveOnEventStoreHandler < EventManager::HandlerBase

      def notify(event)
        Event.create( event_name: event.name,
                      event_data: event.event_data,
                      obj_id: event.event_object_id,
                      obj_type: event.event_object_type )
      end

    end

  end
end
