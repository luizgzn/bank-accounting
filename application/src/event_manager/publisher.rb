module EventManager
  class Publisher

    def initialize
      @subscribers = {}
    end

    def configure_handlers(&block)
      instance_eval(&block)
    end

    def subscribe(event_name, handlers)
      @subscribers[event_name] = handlers
    end

    def publish(event)
      unless @subscribers.keys.include?(event.name)
        Rails.logger.error "[EventManager::Publisher] Event not subscribed event #{event.name}"

        return false
      end

      handlers = @subscribers[event.name] || []
      handlers.each do |handler|
        handler.send_notification(event)
      end

    end

  end
end
