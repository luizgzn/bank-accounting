module EventManager
  class HandlerBase

    def initialize
      @logger = Rails.logger
    end

    def notify event
      raise NotImplementedError.new "Method notify not implemented at #{self.class.name}"
    end

    def send_notification event
      begin
        log_start(event)
        notify(event)
        log_end
      rescue => e
        log_error(e)
      end
    end

    def log_start event
      @logger.info "Handler #{self.class.name} for Event #{event.name} | Started "
    end

    def log_end
      @logger.info "Handler #{self.class.name} | Finished"
    end

    def log_error error
      @logger.error "Handler #{self.class.name} | Error occured "
      @logger.error error
      error.backtrace.each { |line| @logger.error line }
    end

  end
end
