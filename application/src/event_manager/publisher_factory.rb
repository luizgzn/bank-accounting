module EventManager
  class PublisherFactory

    def self.build_with_subscribers
      publisher = EventManager::Publisher.new
      publisher.configure_handlers do |config|
        config.subscribe( AccountsManagement::Events::TransferMoney::NAME,     [ EventManager::Handlers::SaveOnEventStoreHandler.new ])
        
        config.subscribe( AccountsManagement::Events::AccountWithdraw::NAME,   [ EventManager::Handlers::SaveOnEventStoreHandler.new ])
        
        config.subscribe( AccountsManagement::Events::AccountDeposit::NAME,    [ EventManager::Handlers::SaveOnEventStoreHandler.new ])
      end
      publisher
    end
    
  end
end
