module EventManager
  class EventBase

    def name
      raise NotImplementedError.new "Method name not implemented at #{self.class.name}"
    end

    def event_object_id
      raise NotImplementedError.new "Method event_object_id not implemented at #{self.class.name}"
    end

    def event_object_type
      raise NotImplementedError.new "Method event_object_type not implemented at #{self.class.name}"
    end

    def event_data
      raise NotImplementedError.new "Method event_data not implemented at #{self.class.name}"
    end

    def full_data
      @data = event_data || {}

      @data
    end

    def to_hash
      {
        event_name: name,
        event_data: full_data,
        object_id: event_object_id,
        object_type: event_object_type
    }
    end

  end
end
