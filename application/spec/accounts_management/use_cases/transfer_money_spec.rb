require_relative '../../spec_helper'

describe AccountsManagement::UseCases::TransferMoney do

  describe "#execute" do

    let(:source_account_id) { 42 }
    let(:destination_account_id) { 4242 }
    let(:amount) { 42424242 }
    let(:source_account) { instance_double("Domain::Account") }
    let(:destination_account) { instance_double("Domain::Account") }
    let(:all_accounts) { instance_double("Repositories::AllAccounts") }
    let(:specifications) {[
      instance_double("Domain::Specifications::OriginalAccountSufficientFunds"),
    ]}
    let(:use_case) {
      described_class.new(
        all_accounts: all_accounts,
        specifications: specifications  
      )
    }

    context "When the original account has sufficient funds" do
      let(:original_account_balance) { 42424243 }

      before(:each) do
        allow(all_accounts).to receive(:transaction).and_yield
        allow(source_account).to receive(:deposit)
        allow(destination_account).to receive(:id)
      end

      it "Transfers the amount form source account to destination account" do
        expect{ |block| all_accounts.transaction(&block) }.to yield_with_no_args
        expect(all_accounts).to receive(:get_by_id).with(source_account_id).and_return(source_account)
        expect(source_account).to receive(:verify!).with(specifications: specifications, amount: amount)
        expect(all_accounts).to receive(:get_by_id).with(destination_account_id).and_return(destination_account)
        expect(source_account).to receive(:withdraw!).with(amount)
        expect(use_case).to receive(:publish_account_withdraw_event).with(account: source_account, amount: amount)
        expect(destination_account).to receive(:deposit!).with(amount)
        expect(use_case).to receive(:publish_account_deposit_event).with(account: destination_account, amount: amount)
        expect(all_accounts).to receive(:save).with(source_account)
        expect(all_accounts).to receive(:save).with(destination_account)
        expect(use_case).to receive(:publish_money_transfer_event).with(
          source_account: source_account,
          destination_account: destination_account,
          amount: amount
        )
      end
      
      after(:each) do
        use_case.execute(
          source_account_id: source_account_id,
          destination_account_id: destination_account_id,
          amount: amount
        )
      end

    end

    context "When the account balance specification is not satisfied" do
      let(:error) { AccountsManagement::Exceptions::InsufficientFundsException }
      before(:each) do
        allow(all_accounts).to receive(:transaction).and_yield
        allow(all_accounts).to receive(:get_by_id).with(source_account_id).and_return(source_account)
        allow(source_account).to receive(:verify!).with(specifications: specifications, amount: amount).and_raise(error)
      end

      it "Gets InsufficientFundsException" do
        expect{         
          use_case.execute(
            source_account_id: source_account_id,
            destination_account_id: destination_account_id,
            amount: amount
          )
        }.to raise_error(error)
      end

    end
  end
end
