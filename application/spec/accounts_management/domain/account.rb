require_relative '../../spec_helper'

describe AccountsManagement::Domain::Account do

  describe "#account" do

    let(:amount) { 42 }
    let(:string) { "42" }
    let(:float) { 42.42 }
    let(:list) { [42] }
    let(:hash) { {value: 42} }
    let(:account) {
      described_class.new(
        id: 1
      )
    }

    context "When you try to deposit something that is not an integer" do
      let(:original_account_balance) { 42424243 }

      before(:each) do
        allow(account).to receive(:deposit!).with(string).and_raise(TypeError)
        allow(account).to receive(:deposit!).with(float).and_raise(TypeError)
        allow(account).to receive(:deposit!).with(list).and_raise(TypeError)
        allow(account).to receive(:deposit!).with(hash).and_raise(TypeError)
      end

      it "Raises a type error" do
        expect{ account.deposit!(string) }.to raise_error(TypeError)
        expect{ account.deposit!(float) }.to raise_error(TypeError)
        expect{ account.deposit!(list) }.to raise_error(TypeError)
        expect{ account.deposit!(hash) }.to raise_error(TypeError)
      end
      
    end
  end
end
