class AddUserIdToAccount < ActiveRecord::Migration[6.0]
  def change
    add_column :accounts, :user_id, :integer
    add_foreign_key :accounts, :users, column: :user_id, primary_key: :id
  end
end
