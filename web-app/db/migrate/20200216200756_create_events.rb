class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.integer :obj_id
      t.string :obj_type
      t.json :event_data
      t.string :event_name

      t.timestamps
    end
  end
end
