# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Account.all.delete_all
User.all.delete_all

10.times do |i|
  user = User.create!(email: "#{i}@a.com", password: "password", username: "usuario_#{i}")
  account = Account.new(balance: 2 ** i, user_id: user.id)
  account.save!
end