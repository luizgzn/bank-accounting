module Api
  module V1
    class AccountController < BaseController
      before_action :authorize_request
      
      def show
        begin
          account_id = account_params[:id]
          verify_account_ownership(account_id: account_id)
          use_case = AccountsManagement::UseCases::GetAccount.build
          account = use_case.execute(account_id: account_id)
          render json: { account_data: account.to_hash }
        rescue AccountsManagement::Exceptions::AccountOwnershipException => e
          render json: { message: e.message }, status: :unauthorized
        rescue AccountsManagement::Exceptions::AccountNotFoundException => e
          Rails.logger.error(e.message)
          render json: { error: e, message: e.message }, status: :not_found
        rescue StandardError => e
          Rails.logger.error(e.message)
          Rails.logger.error(e.backtrace.join("\n"))
          render json: { error: e, message: e.message }, status: :bad_request
        end
      end

      private

      def account_params
        params.permit(:id, :balance)
      end

    end
  end
end