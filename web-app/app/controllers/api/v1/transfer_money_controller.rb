module Api
  module V1
    class TransferMoneyController < BaseController
      before_action :authorize_request
      
      def create
        begin
          source_account_id = params[:source_account_id]
          verify_account_ownership(account_id: source_account_id)
          use_case = AccountsManagement::UseCases::TransferMoney.build
          data = use_case.execute(
            source_account_id: source_account_id,
            destination_account_id: transfer_params[:destination_account_id],
            amount: transfer_params[:amount].to_i
          )
          render json: {data: data}
        rescue AccountsManagement::Exceptions::InsufficientFundsException => e
          render json: { message: e.message }, status: :bad_request
        rescue AccountsManagement::Exceptions::AccountOwnershipException => e
          render json: { message: e.message }, status: :unauthorized
        rescue ActionDispatch::Http::Parameters::ParseError => e 
          render json: { message: e.message }, status: :bad_request
        rescue StandardError => e
          Rails.logger.error(e.message)
          Rails.logger.error(e.backtrace.join("\n"))
          render json: { message: e.message }, status: :bad_request
        end
      end

      private

      def transfer_params
        params.permit(:source_account_id, :destination_account_id, :amount)
      end
    end
  end
end