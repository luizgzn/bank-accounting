class User < ApplicationRecord

  has_secure_password

  has_many :accounts

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, length: { minimum: 6 }, if: -> { new_record? || !password.nil? }

  def validate_account_ownership(account_id)
    self.accounts.pluck(:id).include?(account_id.to_i)
  end

end
