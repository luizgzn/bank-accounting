module AuthenticationHelper

  def authorize_request
    begin
      find_current_user
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end

  def sign_in(email:, password:)
    user = User.find_by_email(params[:email])
    if user.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: user.id)
      render json: { token: token, username: user.username }, status: :ok
    else
      render json: { error: 'Unauthorized' }, status: :unauthorized
    end
  end

  def verify_account_ownership(account_id:)
    current_user = find_current_user
    unless current_user.validate_account_ownership(account_id)
      raise AccountsManagement::Exceptions::AccountOwnershipException.new("You are not allowed to perform this transaction.")
    end
  end

  private

  def find_current_user
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    decoded = JsonWebToken.decode(header)
    User.find(decoded[:user_id])
  end

end