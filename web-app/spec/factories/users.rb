FactoryBot.define do
  factory :user, class: User do
    email { Faker::Internet::email }
    name { Faker::Name.name }
    password {'123456'}
  end
end