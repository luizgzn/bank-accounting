FactoryBot.define do
  factory :account, class: Account do
    balance { 42000000 }
    user
  end
end