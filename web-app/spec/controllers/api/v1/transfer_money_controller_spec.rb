require 'rails_helper'

RSpec.describe Api::V1::TransferMoneyController, type: :controller do

  describe "POST create" do

    before(:all) do
      Account.destroy_all
      User.destroy_all
      @user = FactoryBot.create(:user)
      @user2 = FactoryBot.create(:user)
      @source_account = FactoryBot.create(:account, user: @user)
      @destination_account = FactoryBot.create(:account, user: @user2)
      @amount = 4242
    end
    
    context "When the source account has enough balance" do
      it "Makes a transfer to destination account" do 
        request.headers['Authorization'] = JsonWebToken.encode(user_id: @user.id)

        post :create, params: {
          source_account_id: @source_account.id,
          destination_account_id: @destination_account.id,
          amount: @amount
        }

        expect(response).to have_http_status(200)
      end
    end

    context "The user has not enough funds to make a transfer" do
      amount = 424242424242424242442
      it "Returns as a bad request" do 
        request.headers['Authorization'] = JsonWebToken.encode(user_id: @user.id)

        post :create, params: {
          source_account_id: @source_account.id,
          destination_account_id: @destination_account.id,
          amount: amount
        }

        expect(response).to have_http_status(400)
      end
    end

    context "The source account does not belong to the user" do
      it "Returns an unauthorized request" do 
        request.headers['Authorization'] = JsonWebToken.encode(user_id: @user.id)

        post :create, params: {
          source_account_id: @destination_account.id,
          destination_account_id: @source_account.id,
          amount: @amount
        }

        expect(response).to have_http_status(401)
      end
    end

    context "The user does not have a valid token" do
      it "Returns an unauthorized request" do 
        request.headers['Authorization'] = 'WoompaLoompa4242'

        post :create, params: {
          source_account_id: @source_account.id,
          destination_account_id: @destination_account.id,
          amount: @amount
        }

        expect(response).to have_http_status(401)
      end
    end

  end
end
