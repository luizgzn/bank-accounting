require 'rails_helper'

RSpec.describe Api::V1::AccountController, type: :controller do
  
  before(:all) do
    Account.destroy_all
    User.destroy_all
    @user = FactoryBot.create(:user)
    @account = FactoryBot.create(:account, user: @user)
  end

  describe "GET account" do
    context "When the source account has enough balance" do
      it "Shows the user account" do 
        request.headers['Authorization'] = JsonWebToken.encode(user_id: @user.id)
        get :show, params: { id: @account.id } 

        expect(response).to have_http_status(200)
      end
    end

    context "When the account id not the users id" do
      it "Returns not found" do 
        request.headers['Authorization'] = JsonWebToken.encode(user_id: @user.id)
        get :show, params: { id: @account.id + 999 } 

        expect(response).to have_http_status(401)
      end
    end

    context "When the token is invalid" do
      it "Returns not authorized" do 
        request.headers['Authorization'] = "WoompaLoompa4242"
        get :show, params: { id: @account.id } 

        expect(response).to have_http_status(401)
      end
    end

  end
end
