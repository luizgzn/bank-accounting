Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :user
      resources :transfer_money, only: :create
      resources :account, only: :show
      post '/auth/login', to: 'authentication#create'
    end
  end
end